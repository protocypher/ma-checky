from setuptools import setup


setup(
      name="Checky",
      version="0.1.0",
      packages=["checky"],
      url="https://bitbucket.org/protocypher/ma-checky",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A todo list application."
)

