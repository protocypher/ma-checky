# Checky

**Todo List**

Checky is a basic, text based, todo list application that uses the `cmd.Cmd`
framework. Tasks can be managed in and out of categories. Categories and be
put into projects. Tasks have scheduling attributes. Projects, categories and
tasks can be exported into interactive reports of different styles.

